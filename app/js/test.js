let dataForm = $('#data-form');
let [forinWord, motherWord, imageSource, submitButton] = [$('#forin-word'),$('#mother-word'),$('#image-source'), $('#submit-button')];
let lesson = [];
const lessonsObject = {
    lesson1: [],
    lesson2: [],
    lesson3: []
}

let [forinText, motherText, imageCard] = [$('.forin-text'),$('.mother-text'),$('.word-image')];

if(localStorage.lesson1 == undefined) {
    lesson = [];
} else {
   lesson = JSON.parse(localStorage.getItem('lesson1'));
   
   lessonsObject.lesson1 = JSON.parse(localStorage.getItem('lesson1'));
   lessonsObject.lesson2 = JSON.parse(localStorage.getItem('lesson2'));
   lessonsObject.lesson3 = JSON.parse(localStorage.getItem('lesson3'));
}

console.log( lessonsObject.lesson2);


function storeData(e) {
    e.preventDefault();

    const cardObject = {
        forin : forinWord.val(),
        mother : motherWord.val(),
        imageSrc : imageSource.val()
    };
    // cardObject.forin = forinWord.val();
    // cardObject.mother = motherWord.val();
    // cardObject.imageSrc = imageSource.val();

    lesson.push(cardObject); 

    localStorage.setItem('lesson1', JSON.stringify(lesson));

    console.log(lesson); 
         

    forinWord.val('');
    motherWord.val('');
    imageSource.val('');

    // showData();
    
}

function showData() {
    console.log(lesson[1].forin);

    forinText.text(lesson[1].forin);
    motherText.text(lesson[1].mother);
    imageCard.attr('src', `img/lesson1/${lesson[1].imageSrc}.jpg`);
    
}

// showData();


submitButton.on('click', storeData);
